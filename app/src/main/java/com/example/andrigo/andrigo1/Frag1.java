package com.example.andrigo.andrigo1;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Frag1 extends Fragment {

    Button btn1;
    EditText etN, etE;

    public Frag1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_frag1,container,false);

        etN = (EditText)view.findViewById(R.id.etNombre);
        etE = (EditText)view.findViewById(R.id.etEdad);
        btn1 = (Button) view.findViewById(R.id.btNextOne);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = etN.getText().toString();
                String old =  etE.getText().toString();

                if(!name.isEmpty() ){

                    FragmentTransaction fr = getFragmentManager().beginTransaction();
                    fr.replace(R.id.fragment_container, new Frag2());
                    fr.commit();

                }else{
                    Toast.makeText(getActivity(), "El nombre es obligatorio", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return view;
    }

}
